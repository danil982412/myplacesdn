
### Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### MyPlacesDN
Данное приложение будет показывать вам близлежащие к вам заведения разного типа.

### Как скачать
Вы можете зайти в репозиторий данного проекта и скачать его тут ----> https://gitlab.com/danil982412/myplacesdn

### Доп. компоненты
для работы с данным ПО вам нужно дополнительно установить на свой ПК:
1. Yarn
2. Git
3. Vue CLI

### Установка проекта
введите команду в консоль: yarn install

###Развертывание
Введите данную команду в консоль: yarn run build